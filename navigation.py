from selenium.webdriver.common.keys import Keys
from selenium import webdriver
import unittest
import time

class Navigate(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Edge(executable_path=r"D:\IMPORTANTE\driver\edgedriver_win64\msedgedriver.exe")
    
    def test_navigate(self):
        driver = self.driver
        driver.get("http://google.com")        
        time.sleep(3)
        driver.execute_script("window.open('');")
        time.sleep(2)
        driver.switch_to.window(driver.window_handles[1])
        driver.get("http://stackoverflow.com")        
        time.sleep(3)
        driver.switch_to.window(driver.window_handles[0])
        time.sleep(3)


# correr la clase 
if __name__ == '__main__':
    unittest.main()