from selenium.webdriver.common.keys import Keys
from selenium import webdriver
import unittest
import time

class TestSearch(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Edge(executable_path=r"D:\IMPORTANTE\driver\edgedriver_win64\msedgedriver.exe")
        
    def test_search(self):
        driver = self.driver
        driver.get("http://google.com")
        
        # valida que tenga la palabra google
        self.assertIn("Google",driver.title)
        
        search = driver.find_element("name","q")
        search.send_keys("selenium")
        
        # RETURN es lo mismo del ENTER
        search.send_keys(Keys.RETURN)
        time.sleep(5)
        
        assert("No se encontró el elemento:" not in driver.page_source)

    def tearDown(self):
        self.driver.close()
        
# correr la clase 
if __name__ == '__main__':
    unittest.main()