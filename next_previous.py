from selenium.webdriver.common.keys import Keys
from selenium import webdriver
import unittest
import time

class NextPrevious(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Edge(executable_path=r"D:\IMPORTANTE\driver\edgedriver_win64\msedgedriver.exe")
        
    def test_next_previous(self):
        driver = self.driver
        driver.get("http://gmail.com")
        time.sleep(3)
        driver.get("http://google.com")
        time.sleep(3)
        driver.get("http://youtube.com")
        time.sleep(3)
        driver.back()
        time.sleep(3)
        driver.back()
        time.sleep(3)
        
        driver.forward()
        time.sleep(3)

# correr la clase 
if __name__ == '__main__':
    unittest.main()
