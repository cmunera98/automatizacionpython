from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import unittest

class ExplicitWait(unittest.TestCase):
   
    def setUp(self):
        self.driver = webdriver.Edge(executable_path=r"D:\IMPORTANTE\driver\edgedriver_win64\msedgedriver.exe")
        
    def test_explicit_wait(self):
        driver = self.driver
        driver.get("http://google.com")
        
        try:
            # cargando el elemento, lo intenta durante 10 veces
            # hasta encontrar el elemento "q" 
            element = WebDriverWait(driver,10).until(EC.presence_of_element_located((By.NAME, "q")))
        finally:
            driver.close()

# correr la clase 
if __name__ == '__main__':
    unittest.main()
        